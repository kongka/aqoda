import fs from 'fs';
import { HotelManager } from './hotelManager';
import { Guest } from './guest';
import { Room } from './room';

class Command {
    name: string;
    parameters: Array<string | number>;

    constructor(name: string, parameters: string[]) {
        this.name = name; 
        this.parameters = this.getNewParameters(parameters);
    }

    getNewParameters(parameters: string[]): Array<string | number> {
        const newParameter = parameters.map(
            parameter => {
                const parsedParameter = parseInt(parameter, 10);

                return Number.isNaN(parsedParameter)
                    ? parameter
                    : parsedParameter
            }
        );

        return newParameter;
    }
}

function main() {
    const fileName: string = 'input.txt';
    const commands: Command[] = getCommandFromFile(fileName);
    const hotelManager = new HotelManager();

    commands.forEach(command => {
        switch (command.name) {
            case 'create_hotel':
                const [numberOfFloor, roomPerFloor] = command.parameters as [number, number];
                
                hotelManager.createHotel(numberOfFloor, roomPerFloor);
                console.log(`Hotel created with ${numberOfFloor} floor(s), ${roomPerFloor} room(s) per floor.`);
                break;
            case 'book': {
                const [roomNumber, guestName, guestAge] = command.parameters as [string, string, number];
                const guest = new Guest(guestName, guestAge);

                try {
                    const room = hotelManager.bookRoom(
                        roomNumber.toString(),
                        guest
                    );

                    console.log(`Room ${roomNumber} is booked by ${guestName} with keycard number ${room!.keycard!.id}.`);
                } catch (error) {
                    console.log(error.message);
                }

                break;
            }
            case 'list_available_rooms': {
                const rooms = hotelManager.roomManager!.findAvailableRooms();
                const roomNumbers = hotelManager.roomManager!.getRoomNumbers(rooms);

                console.log(roomNumbers.join(', '));
                break;
            }
            case 'checkout': {
                const [keycardId, guestName] = command.parameters as [string, string];
                
                try {
                    const room = hotelManager.checkout(keycardId.toString(), guestName);

                    console.log(`Room ${room!.number} is checkout.`);
                } catch(error) {
                    console.log(error.message);
                }
                break;
            }
            case 'list_guest': {
                const guests = hotelManager.guestManager!.listGuests();
                const guestNames = hotelManager.guestManager!.getGuestNames(guests);

                console.log(guestNames.join(', '));
                break;
            }
            case 'get_guest_in_room': {
                const [roomNumber] = command.parameters;
                const guest = hotelManager.bookingDetailManager!.findGuestByRoom(roomNumber.toString());

                console.log(guest.name);
                break;
            }
            case 'list_guest_by_age': {
                const [condition, targetAge] = command.parameters as [string, number];
                const guests = hotelManager.guestManager!.findGuestsByAgeCondition(
                    condition,
                    targetAge
                );
                const guestNames = hotelManager.guestManager!.getGuestNames(guests!);

                console.log(guestNames.join(', '));
                break;
            }
            case 'list_guest_by_floor': {
                const [floorNumber] = command.parameters as [number];
                const guests = hotelManager.bookingDetailManager!.findGuestsByFloor(floorNumber);
                const guestNames = guests
                    .map(guest => guest.name)
                    .join(', ')

                console.log(guestNames);
                break;
            }
            case 'checkout_guest_by_floor': {
                const [floorNumber] = command.parameters as [number];
                const rooms = hotelManager.checkoutByFloor(floorNumber);
                const roomNumbers = hotelManager.roomManager!.getRoomNumbers(rooms); 

                if (rooms) {
                    console.log(`Room ${roomNumbers.join(',')} are checkout.`);
                }
                break;
            }
            case 'book_by_floor': {
                const [floor, guestName, guestAge] = command.parameters as [number, string, number];
                const guest = new Guest(guestName, guestAge);
                try {
                    const { rooms, keycardIDs } = hotelManager.bookByFloor(
                        floor,
                        guest
                    );
                    const roomNumbers = rooms
                        .map((room: Room) => room.number)
                        .join(', ');

                    console.log(`Room ${roomNumbers} are booked with keycard number ${keycardIDs.join(', ')}`);
                } catch(error) {
                    console.log(error.message);
                }
                break;
            }
            default:
                break;
        }
    });
}

function getCommandFromFile(fileName: string): Command[] {
    const file = fs.readFileSync(fileName, 'utf-8');

    return file.split('\r\n')
        .map(line => line.split(' '))
        .map(([commandName, ...parameters]) => new Command(commandName, parameters));
}

main();