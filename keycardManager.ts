import { Keycard } from './keycard';
import { Room } from './room';

export class KeycardManager {
    keycards?: Keycard[];

    constructor(floorCount: number, roomPerFloor: number) {
        this.keycards = this.createKeycards(floorCount, roomPerFloor);
    }

    createKeycards(floorCount: number, roomPerFloor: number): Keycard[] {
        const keycardCount = floorCount * roomPerFloor;
        const keycards = Array.from(
            { length: keycardCount },
            (_, keycardIndex) => {
                const id: string = (keycardIndex + 1).toString();

                return new Keycard(id);
            }
        )

        return keycards;
    }

    findKeycardById(keycardId: string) {
        return this.keycards!.find(
            keycard => keycard.id === keycardId
        );
    }

    findKeycardByRoom(room: Room) {
        return this.keycards!.find(
            keycard => keycard.room === room
        );
    }

    findUnheldKeycard() {
        return this.keycards!.find(
            keycard => !keycard.isHeld
        );
    }
}

module.exports.KeycardManager = KeycardManager;