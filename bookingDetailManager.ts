import { BookingDetail } from './bookingDetail'; 
import { Keycard } from './keycard';
import { Room } from './room';
import { Guest } from './guest';

export class BookingDetailManager {
    bookingDetails: BookingDetail[];
    
    constructor() {
        this.bookingDetails = [];
    }

    removeBookingDetail(room: Room, keycard: Keycard) {
        this.bookingDetails = this.bookingDetails!.filter(
            (detail: BookingDetail) => detail.room !== room && detail.keycard !== keycard
        );
    }

    recordBookingDetail(guest: Guest, room: Room, keycard: Keycard) {
        const bookingDetail = new BookingDetail(
            guest,
            room,
            keycard,
        );
        
        this.bookingDetails!.push(bookingDetail);
    }

    findGuestByRoom(roomNumber: string): Guest {
        return this.bookingDetails.find(
            detail => detail.roomNumber === roomNumber)!.guest;
    }

    findGuestsByFloor(floorNumber: number) {
        return this.bookingDetails
            .filter(detail => detail.room.floor === floorNumber)
            .map(detail => detail.guest);
    }
}

module.exports.BookingDetailManager = BookingDetailManager;