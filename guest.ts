import { Keycard } from "./keycard";

export class Guest {
    name: string;
    age: number;
    keycards: Keycard[]

    constructor(name: string, age: number) {
        this.name = name;
        this.age = age;
        this.keycards = [];
    }

    get isStayingInHotel() {
        return this.keycards.length > 0;
    }

    addKeycard(keycard: Keycard) {
        this.keycards.push(keycard);
    }

    removeKeycard(keycardToBeRemoved: Keycard | undefined) {
        this.keycards = this.keycards.filter(
            keycard => keycard !== keycardToBeRemoved
        );
    }
}

module.exports.Guest = Guest;