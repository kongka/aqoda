import { Guest } from "./guest";
import { Room } from "./room";

export class Keycard {
    id: string;
    holder?: Guest;
    room?: Room;

    constructor(id: string) {
        this.id = id;
        this.holder = undefined;
        this.room = undefined;
    }

    get isHeld() {
        return this.holder !== undefined;
    }
    
    register(holder: Guest, room: Room) {
        this.holder = holder;
        this.room = room;
    }
    
    unregister() {                              
        this.holder = undefined;
        this.room = undefined;
    }
}

module.exports.Keycard = Keycard;
