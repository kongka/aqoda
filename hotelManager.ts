import { Hotel } from './hotel';
import { RoomManager } from './roomManager';
import { GuestManager } from './guestManager';
import { KeycardManager } from './keycardManager';
import { BookingDetailManager } from './bookingDetailManager';
import { Guest } from './guest';
import { Room } from './room';
import { Keycard } from './keycard';

export class HotelManager {
  hotel?: Hotel;
  roomManager?: RoomManager;
  keycardManager?: KeycardManager;
  bookingDetailManager?: BookingDetailManager;
  guestManager?: GuestManager;

  createHotel(floorCount: number, roomPerFloor: number): void {
    this.hotel = new Hotel(floorCount, roomPerFloor);
    this.roomManager = new RoomManager(floorCount, roomPerFloor);
    this.keycardManager = new KeycardManager(floorCount, roomPerFloor);
    this.bookingDetailManager = new BookingDetailManager();
    this.guestManager = new GuestManager();
  }

  bookRoom(roomNumber: string, guest: Guest) {
    const selectedRoom = this.roomManager!.findRoomByRoomNumber(roomNumber);

    if (this.isHotelFull()) {
      console.log("Hotel is fully booked");

      return;
    }
    
    if (selectedRoom!.isBooked) {
      throw new Error(`Cannot book room ${roomNumber} for ${guest.name}, The room is currently booked by ${selectedRoom!.guest!.name}.`);
    }
    const keycard = this.keycardManager!.findUnheldKeycard();

    this.checkin(
      guest,
      selectedRoom!,
      keycard!
    );

    return selectedRoom;
  }

  checkout(keycardId: string, guestName: string) {
    const keycard = this.keycardManager!.findKeycardById(keycardId);
    
    if (keycard!.holder!.name !== guestName) {
      throw new Error(`Only ${keycard!.holder!.name} can checkout with keycard number ${keycardId}.`)
    }
    const room = this.roomManager!.findRoomByKeycard(keycard!);

    this.clearInfo(room!, keycard!);

    return room;
  }

  bookByFloor(floorNumber: number, guest: Guest) {
    const rooms = this.roomManager!.getAvailableRoomsByFloor(floorNumber);

    if (this.isFloorNotBookable(floorNumber)) {
      throw new Error(`Cannot book floor ${floorNumber} for ${guest.name}.`);
    }

    rooms.forEach(
      (room: Room) => {
        this.bookRoom(room.number, guest);
      });
    
    const keycardIDs = rooms.map((room: Room) => room.keycard!.id);

    return { rooms, keycardIDs };
  }

  checkoutByFloor(floorNumber: number) {
    const rooms = this.roomManager!.getUnavailableRoomsByFloor(floorNumber);

    rooms.forEach((room: Room) => {
      this.checkoutEachRoom(room);
    });

    return rooms;
  }

  // Private

  checkin(guest: Guest, room: Room, keycard: Keycard) {
    guest = this.guestManager!.checkGuest(guest);

    keycard.register(guest, room);
    guest.addKeycard(keycard);
    room.book(guest, keycard);

    this.bookingDetailManager!.recordBookingDetail(
      guest,
      room,
      keycard
    );
  }

  clearInfo(room: Room, keycard: Keycard) {
    const guest = room.guest!.removeKeycard(keycard);
    this.bookingDetailManager!.removeBookingDetail(room, keycard);
    
    keycard.unregister();
    room.unbook();
  }

  checkoutEachRoom(room: Room) {
    const keycard = this.keycardManager!.findKeycardByRoom(room);

    this.clearInfo(room, keycard!);
  }

  isFloorNotBookable(floorNumber: number) {
    return this.roomManager!.rooms
      .filter(room => room.floor === floorNumber)
      .some(room => room.isBooked);
  }

  isHotelFull() {
    return this.roomManager!.rooms.every(room => room.isBooked);
  }
}

module.exports.HotelManager = HotelManager;