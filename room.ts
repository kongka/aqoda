import { Keycard } from "./keycard";
import { Guest } from "./guest";

export class Room {
    floor: number;
    number: string;
    keycard?: Keycard;
    guest?: Guest;
    
    constructor(floor: number, number: number) {
        this.floor = floor;
        this.number = this.getRoomNumber(number);
        this.keycard = undefined;
        this.guest = undefined;
    }

    getRoomNumber(number: number): string {
        const floorNumber = this.floor;
        const roomNumber = number.toString().padStart(2, '0');
        
        return floorNumber + roomNumber;
    }

    get isBooked(): Boolean {
        return this.guest !== undefined;
    }

    book(guest: Guest, keycard: Keycard) {
        this.guest = guest;
        this.keycard = keycard;
    }

    unbook() {
        this.guest!.removeKeycard(this.keycard);
        this.guest = undefined;
        this.keycard = undefined;
    }
}

module.exports.Room = Room;