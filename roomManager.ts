import { Room } from './room.js';
import { Keycard } from './keycard.js';

export class RoomManager {
    rooms: Room[];

    constructor(floorCount: number, roomPerFloor: number) {
        this.rooms = this.createRooms(floorCount, roomPerFloor);
    }

    createRooms(floorCount: number, roomPerFloor: number) {
        const rooms = Array.from(
            { length: floorCount },
            (_, floorIndex) =>
                Array.from(
                    { length: roomPerFloor },
                    (_, roomIndex) =>
                        new Room(floorIndex + 1, roomIndex + 1)
            )
        );

        return rooms.flat();
    }
    
    findRoomByRoomNumber(roomNumber: string) {
        return this.rooms.find(
            room => room.number === roomNumber
        );
    }

    findAvailableRoomByRoomNumber(roomNumber: string) {
        return this.rooms.find(
            room => room.number === roomNumber && !room.isBooked
        );
    }

    getRoomsByFloor(floorNumber: number) {
        return this.rooms.filter(
            room => room.floor === floorNumber
        );
    } 

    getAvailableRoomsByFloor(floorNumber: number) {
        return this.getRoomsByFloor(floorNumber).filter(
            room => !room.isBooked
        );
    }

    getUnavailableRoomsByFloor(floorNumber: number) {
        return this.getRoomsByFloor(floorNumber).filter(
            room => room.isBooked
        );
    }

    findAvailableRooms() {
        return this.rooms.filter(
            room => !room.isBooked
        );
    }

    findRoomByKeycard(keycard: Keycard) {
        return this.rooms.find(
            room => room.keycard === keycard
        );
    }

    getRoomNumbers(rooms: Room[]) {
        return rooms.map(checkoutRoom => checkoutRoom.number)
    }
}

module.exports.RoomManager = RoomManager;