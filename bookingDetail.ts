import { Guest } from "./guest";
import { Room } from "./room";
import { Keycard } from "./keycard";

export class BookingDetail {
    guest: Guest
    room: Room
    keycard: Keycard

    constructor(guest: Guest, room: Room, keycard: Keycard) {
        this.guest = guest;
        this.room = room;
        this.keycard = keycard;
    }

    get guestName() {
        return this.guest.name;
    }

    get roomNumber() {
        return this.room.number;
    }

    get floorNumber(){
        return this.room.floor;
    }

    get keycardId() {
        return this.keycard.id;
    }
}

module.exports.BookingDetail = BookingDetail;