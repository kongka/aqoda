//TODO: Move these function to related classes.
import { Keycard } from './keycard';
import { Room } from './room';

export class Hotel {
    floorCount: number;
    roomPerFloor: number;

    constructor(floorCount: number, roomPerFloor: number) {
        this.floorCount = floorCount;
        this.roomPerFloor = roomPerFloor;
    }

    createRooms(floorCount: number, roomPerFloor: number) {
        const rooms = Array.from(
            { length: floorCount },
            (_, floorIndex) =>
                Array.from(
                    { length: roomPerFloor },
                    (_, roomIndex) =>
                        new Room(floorIndex + 1, roomIndex + 1)
            )
        );

        return rooms.flat();
    }

    createKeycards() {
        const keycardCount = this.floorCount * this.roomPerFloor;
        const keycards = Array.from(
            { length: keycardCount },
            (_, keycardIndex) => {
                const id: string = (keycardIndex + 1).toString();

                return new Keycard(id);
            }
        )

        return keycards;
    }
}

module.exports.Hotel = Hotel;