import express, { Request, Response } from 'express';
import { request } from 'https';
import bodyParser = require('body-parser');

const app = express();
const port = 3000;

app.use(bodyParser());

app.get('/', (request: Request, response: Response) => {
    response.send('Hello world');
})

app.get('/welcome', (request: Request, response: Response) => {
    // welcome?name=
    const { name } = request.query;
    response.send(`Welcome ${name}`);
})

app.get('/rooms/:number', (request: Request, response: Response) => {
    const { number } = request.params
    response.send(`This is room ${number}`);
}) 

app.post('/book_room', (request: Request, response: Response) => {
    const body = request.body;
    
    response.json(body);
})

app.listen(port, () => console.log(`App is lisening on port ${port}`))