"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const room_js_1 = require("./room.js");
class RoomManager {
    constructor(floorCount, roomPerFloor) {
        this.rooms = this.createRooms(floorCount, roomPerFloor);
    }
    createRooms(floorCount, roomPerFloor) {
        const rooms = Array.from({ length: floorCount }, (_, floorIndex) => Array.from({ length: roomPerFloor }, (_, roomIndex) => new room_js_1.Room(floorIndex + 1, roomIndex + 1)));
        return rooms.flat();
    }
    findRoomByRoomNumber(roomNumber) {
        return this.rooms.find(room => room.number === roomNumber);
    }
    findAvailableRoomByRoomNumber(roomNumber) {
        return this.rooms.find(room => room.number === roomNumber && !room.isBooked);
    }
    getRoomsByFloor(floorNumber) {
        return this.rooms.filter(room => room.floor === floorNumber);
    }
    getAvailableRoomsByFloor(floorNumber) {
        return this.getRoomsByFloor(floorNumber).filter(room => !room.isBooked);
    }
    getUnavailableRoomsByFloor(floorNumber) {
        return this.getRoomsByFloor(floorNumber).filter(room => room.isBooked);
    }
    findAvailableRooms() {
        return this.rooms.filter(room => !room.isBooked);
    }
    findRoomByKeycard(keycard) {
        return this.rooms.find(room => room.keycard === keycard);
    }
    getRoomNumbers(rooms) {
        return rooms.map(checkoutRoom => checkoutRoom.number);
    }
}
exports.RoomManager = RoomManager;
module.exports.RoomManager = RoomManager;
