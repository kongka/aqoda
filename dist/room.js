"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Room {
    constructor(floor, number) {
        this.floor = floor;
        this.number = this.getRoomNumber(number);
        this.keycard = undefined;
        this.guest = undefined;
    }
    getRoomNumber(number) {
        const floorNumber = this.floor;
        const roomNumber = number.toString().padStart(2, '0');
        return floorNumber + roomNumber;
    }
    get isBooked() {
        return this.guest !== undefined;
    }
    book(guest, keycard) {
        this.guest = guest;
        this.keycard = keycard;
    }
    unbook() {
        this.guest.removeKeycard(this.keycard);
        this.guest = undefined;
        this.keycard = undefined;
    }
}
exports.Room = Room;
module.exports.Room = Room;
