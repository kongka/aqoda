"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BookingDetail {
    constructor(guest, room, keycard) {
        this.guest = guest;
        this.room = room;
        this.keycard = keycard;
    }
    get guestName() {
        return this.guest.name;
    }
    get roomNumber() {
        return this.room.number;
    }
    get floorNumber() {
        return this.room.floor;
    }
    get keycardId() {
        return this.keycard.id;
    }
}
exports.BookingDetail = BookingDetail;
module.exports.BookingDetail = BookingDetail;
