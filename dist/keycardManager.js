"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const keycard_1 = require("./keycard");
class KeycardManager {
    constructor(floorCount, roomPerFloor) {
        this.keycards = this.createKeycards(floorCount, roomPerFloor);
    }
    createKeycards(floorCount, roomPerFloor) {
        const keycardCount = floorCount * roomPerFloor;
        const keycards = Array.from({ length: keycardCount }, (_, keycardIndex) => {
            const id = (keycardIndex + 1).toString();
            return new keycard_1.Keycard(id);
        });
        return keycards;
    }
    findKeycardById(keycardId) {
        return this.keycards.find(keycard => keycard.id === keycardId);
    }
    findKeycardByRoom(room) {
        return this.keycards.find(keycard => keycard.room === room);
    }
    findUnheldKeycard() {
        return this.keycards.find(keycard => !keycard.isHeld);
    }
}
exports.KeycardManager = KeycardManager;
module.exports.KeycardManager = KeycardManager;
