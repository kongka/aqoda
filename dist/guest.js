"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Guest {
    constructor(name, age) {
        this.name = name;
        this.age = age;
        this.keycards = [];
    }
    get isStayingInHotel() {
        return this.keycards.length > 0;
    }
    addKeycard(keycard) {
        this.keycards.push(keycard);
    }
    removeKeycard(keycardToBeRemoved) {
        this.keycards = this.keycards.filter(keycard => keycard !== keycardToBeRemoved);
    }
}
exports.Guest = Guest;
module.exports.Guest = Guest;
