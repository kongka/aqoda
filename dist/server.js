"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const bodyParser = require("body-parser");
const app = express_1.default();
const port = 3000;
app.use(bodyParser());
app.get('/', (request, response) => {
    response.send('Hello world');
});
app.get('/welcome', (request, response) => {
    // welcome?name=
    const { name } = request.query;
    response.send(`Welcome ${name}`);
});
app.get('/rooms/:number', (request, response) => {
    const { number } = request.params;
    response.send(`This is room ${number}`);
});
app.post('/book_room', (request, response) => {
    const body = request.body;
    response.json(body);
});
app.listen(port, () => console.log(`App is lisening on port ${port}`));
