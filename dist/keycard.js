"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Keycard {
    constructor(id) {
        this.id = id;
        this.holder = undefined;
        this.room = undefined;
    }
    get isHeld() {
        return this.holder !== undefined;
    }
    register(holder, room) {
        this.holder = holder;
        this.room = room;
    }
    unregister() {
        this.holder = undefined;
        this.room = undefined;
    }
}
exports.Keycard = Keycard;
module.exports.Keycard = Keycard;
