"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bookingDetail_1 = require("./bookingDetail");
class BookingDetailManager {
    constructor() {
        this.bookingDetails = [];
    }
    removeBookingDetail(room, keycard) {
        this.bookingDetails = this.bookingDetails.filter((detail) => detail.room !== room && detail.keycard !== keycard);
    }
    recordBookingDetail(guest, room, keycard) {
        const bookingDetail = new bookingDetail_1.BookingDetail(guest, room, keycard);
        this.bookingDetails.push(bookingDetail);
    }
    findGuestByRoom(roomNumber) {
        return this.bookingDetails.find(detail => detail.roomNumber === roomNumber).guest;
    }
    findGuestsByFloor(floorNumber) {
        return this.bookingDetails
            .filter(detail => detail.room.floor === floorNumber)
            .map(detail => detail.guest);
    }
}
exports.BookingDetailManager = BookingDetailManager;
module.exports.BookingDetailManager = BookingDetailManager;
