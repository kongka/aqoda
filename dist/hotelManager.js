"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const hotel_1 = require("./hotel");
const roomManager_1 = require("./roomManager");
const guestManager_1 = require("./guestManager");
const keycardManager_1 = require("./keycardManager");
const bookingDetailManager_1 = require("./bookingDetailManager");
class HotelManager {
    createHotel(floorCount, roomPerFloor) {
        this.hotel = new hotel_1.Hotel(floorCount, roomPerFloor);
        this.roomManager = new roomManager_1.RoomManager(floorCount, roomPerFloor);
        this.keycardManager = new keycardManager_1.KeycardManager(floorCount, roomPerFloor);
        this.bookingDetailManager = new bookingDetailManager_1.BookingDetailManager();
        this.guestManager = new guestManager_1.GuestManager();
    }
    bookRoom(roomNumber, guest) {
        const selectedRoom = this.roomManager.findRoomByRoomNumber(roomNumber);
        if (this.isHotelFull()) {
            console.log("Hotel is fully booked");
            return;
        }
        if (selectedRoom.isBooked) {
            throw new Error(`Cannot book room ${roomNumber} for ${guest.name}, The room is currently booked by ${selectedRoom.guest.name}.`);
        }
        const keycard = this.keycardManager.findUnheldKeycard();
        this.checkin(guest, selectedRoom, keycard);
        return selectedRoom;
    }
    checkout(keycardId, guestName) {
        const keycard = this.keycardManager.findKeycardById(keycardId);
        if (keycard.holder.name !== guestName) {
            throw new Error(`Only ${keycard.holder.name} can checkout with keycard number ${keycardId}.`);
        }
        const room = this.roomManager.findRoomByKeycard(keycard);
        this.clearInfo(room, keycard);
        return room;
    }
    bookByFloor(floorNumber, guest) {
        const rooms = this.roomManager.getAvailableRoomsByFloor(floorNumber);
        if (this.isFloorNotBookable(floorNumber)) {
            throw new Error(`Cannot book floor ${floorNumber} for ${guest.name}.`);
        }
        rooms.forEach((room) => {
            this.bookRoom(room.number, guest);
        });
        const keycardIDs = rooms.map((room) => room.keycard.id);
        return { rooms, keycardIDs };
    }
    checkoutByFloor(floorNumber) {
        const rooms = this.roomManager.getUnavailableRoomsByFloor(floorNumber);
        rooms.forEach((room) => {
            this.checkoutEachRoom(room);
        });
        return rooms;
    }
    // Private
    checkin(guest, room, keycard) {
        guest = this.guestManager.checkGuest(guest);
        keycard.register(guest, room);
        guest.addKeycard(keycard);
        room.book(guest, keycard);
        this.bookingDetailManager.recordBookingDetail(guest, room, keycard);
    }
    clearInfo(room, keycard) {
        const guest = room.guest.removeKeycard(keycard);
        this.bookingDetailManager.removeBookingDetail(room, keycard);
        keycard.unregister();
        room.unbook();
    }
    checkoutEachRoom(room) {
        const keycard = this.keycardManager.findKeycardByRoom(room);
        this.clearInfo(room, keycard);
    }
    isFloorNotBookable(floorNumber) {
        return this.roomManager.rooms
            .filter(room => room.floor === floorNumber)
            .some(room => room.isBooked);
    }
    isHotelFull() {
        return this.roomManager.rooms.every(room => room.isBooked);
    }
}
exports.HotelManager = HotelManager;
module.exports.HotelManager = HotelManager;
