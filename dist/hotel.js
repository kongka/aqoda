"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//TODO: Move these function to related classes.
const keycard_1 = require("./keycard");
const room_1 = require("./room");
class Hotel {
    constructor(floorCount, roomPerFloor) {
        this.floorCount = floorCount;
        this.roomPerFloor = roomPerFloor;
    }
    createRooms(floorCount, roomPerFloor) {
        const rooms = Array.from({ length: floorCount }, (_, floorIndex) => Array.from({ length: roomPerFloor }, (_, roomIndex) => new room_1.Room(floorIndex + 1, roomIndex + 1)));
        return rooms.flat();
    }
    createKeycards() {
        const keycardCount = this.floorCount * this.roomPerFloor;
        const keycards = Array.from({ length: keycardCount }, (_, keycardIndex) => {
            const id = (keycardIndex + 1).toString();
            return new keycard_1.Keycard(id);
        });
        return keycards;
    }
}
exports.Hotel = Hotel;
module.exports.Hotel = Hotel;
