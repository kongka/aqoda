"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const guest_1 = require("./guest");
class GuestManager {
    constructor() {
        this.guests = [];
    }
    listGuests() {
        this.updateGuests();
        return this.guests.filter(guest => guest.isStayingInHotel);
    }
    updateGuests() {
        this.guests = this.guests.filter(guest => guest.keycards.length > 0);
    }
    getGuestNames(guests) {
        return guests.map(guest => guest.name);
    }
    findGuestsByAgeCondition(condition, targetAge) {
        this.updateGuests();
        switch (condition) {
            case '<':
                return this.guests.filter(guest => guest.age < targetAge);
            case '=':
                return this.guests.filter(guest => guest.age === targetAge);
            case '>':
                return this.guests.filter(guest => guest.age > targetAge);
        }
    }
    checkGuest(guest) {
        if (this.isNewGuest(guest)) {
            const newGuest = new guest_1.Guest(guest.name, guest.age);
            this.guests.push(newGuest);
            return newGuest;
        }
        return guest;
    }
    isNewGuest(checkingGuest) {
        return this.guests.every(guest => guest.name !== checkingGuest.name && guest.age !== checkingGuest.age);
    }
}
exports.GuestManager = GuestManager;
module.exports.GuestManager = GuestManager;
