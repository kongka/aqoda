import { Guest } from './guest';
export class GuestManager {
    guests?: Guest[];

    constructor() {
        this.guests = [];
    }

    listGuests() {
        this.updateGuests();

        return this.guests!.filter(
            guest => guest.isStayingInHotel
        );
    }

    updateGuests() {
        this.guests = this.guests!.filter(
            guest => guest.keycards.length > 0
        );
    }

    getGuestNames(guests: Guest[]) {
        return guests.map(guest => guest.name)
    }

    findGuestsByAgeCondition(condition: string, targetAge: number) {
        this.updateGuests();

        switch (condition) {
            case '<':
                return this.guests!.filter(guest => guest.age < targetAge);
            case '=':
                return this.guests!.filter(guest => guest.age === targetAge);
            case '>':
                return this.guests!.filter(guest => guest.age > targetAge);
        }
    }

    checkGuest(guest: Guest) {
        if (this.isNewGuest(guest)) {
            const newGuest = new Guest(guest.name, guest.age);

            this.guests!.push(newGuest);

            return newGuest;
        }

        return guest;
    }

    isNewGuest(checkingGuest: Guest) {
        return this.guests!.every(
            guest => guest.name !== checkingGuest.name && guest.age !== checkingGuest.age
        );
    }
}

module.exports.GuestManager = GuestManager;